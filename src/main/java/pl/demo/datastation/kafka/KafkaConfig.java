package pl.demo.datastation.kafka;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import pl.demo.datastation.kafka.serde.BetSlipSnapshotSerializer;
import pl.demo.datastation.kafka.serde.MatchSnapshotSerializer;
import pl.demo.datastation.model.betslip.BetSlipSnapshot;
import pl.demo.datastation.model.match.MatchSnapshot;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


@Configuration
public class KafkaConfig {
    private KafkaProperties kafkaProperties;
    private Map<String, Object> props;
    @Autowired
    public KafkaConfig(KafkaProperties kafkaProperties) {
        this.kafkaProperties = kafkaProperties;
    }


    public Properties getKafkaProperties(){
        Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, kafkaProperties.getApplicationId());
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getServers());
   //     properties.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, kafkaProperties.getConcurrency());
        return properties;
    }
    @Bean(name = "matchSnapshotKafkaTemplate")
    public KafkaTemplate<Long, MatchSnapshot> matchSnapshotKafkaTemplate(){
        props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getServers());
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, MatchSnapshotSerializer.class);
        return new KafkaTemplate<>(new DefaultKafkaProducerFactory<>(props));
    }

    @Bean(name = "betSlipSnapshotKafkaTemplate")
    public KafkaTemplate<Long, BetSlipSnapshot> betSlipSnapshotKafkaTemplate(){
        props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getServers());
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, BetSlipSnapshotSerializer.class);
        return new KafkaTemplate<>(new DefaultKafkaProducerFactory<>(props));
    }

}
