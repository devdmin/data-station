package pl.demo.datastation.kafka.listener;


import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import pl.demo.datastation.kafka.KafkaConfig;
import pl.demo.datastation.model.betslip.BetSlipSnapshot;
import pl.demo.datastation.model.match.MatchSnapshot;

import javax.annotation.PostConstruct;

@Service
public class ResultStreamListener {
    private static final Logger LOG = LoggerFactory.getLogger(ResultStreamListener.class);
    private final ResultStreamTopology topologyFactory;
    private final KafkaConfig kafkaConfig;

    private KafkaStreams kafkaStreams;

    @Autowired
    public ResultStreamListener(ResultStreamTopology topologyFactory, KafkaConfig kafkaConfig) {
        this.topologyFactory = topologyFactory;
        this.kafkaConfig = kafkaConfig;
    }

    @PostConstruct
    public void startStream(){createStream(); }


    private void createStream(){
        kafkaStreams = new KafkaStreams(topologyFactory.resultStreamTopology(), kafkaConfig.getKafkaProperties());
        kafkaStreams.start();
    }

    @Bean @Lazy
    public ReadOnlyKeyValueStore<Long, BetSlipSnapshot> betSlipSnapshotReadOnlyKeyValueStore(){
        return kafkaStreams.store("bet-slip-store", QueryableStoreTypes.keyValueStore());
    }

    @Bean @Lazy
    public ReadOnlyKeyValueStore<Long, MatchSnapshot> matchSnapshotReadOnlyKeyValueStore(){
        return kafkaStreams.store("match-store", QueryableStoreTypes.keyValueStore());
    }
}
