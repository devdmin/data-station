package pl.demo.datastation.kafka.listener;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.demo.datastation.kafka.KafkaProperties;
import pl.demo.datastation.kafka.serde.BetSlipSnapshotSerde;
import pl.demo.datastation.kafka.serde.MatchSnapshotSerde;
import pl.demo.datastation.model.betslip.BetSlipSnapshot;
import pl.demo.datastation.model.match.MatchSnapshot;

@Component
public class ResultStreamTopology {
    private static final Logger LOG = LoggerFactory.getLogger(ResultStreamTopology.class);

    private KafkaProperties kafkaProperties;

    @Autowired
    public ResultStreamTopology(KafkaProperties kafkaProperties) {
        this.kafkaProperties = kafkaProperties;
    }



    @Autowired
    public Topology resultStreamTopology(){
       final StreamsBuilder builder = new StreamsBuilder();
        addGlobalMatchStore(builder, kafkaProperties.getStore().getMatch());
        addGlobalBetSlipStore(builder, kafkaProperties.getStore().getBetslip());


        builder.stream(kafkaProperties.getTopic().getMatch(), Consumed.with(Serdes.Long(), MatchSnapshotSerde.serde()));
        builder.stream(kafkaProperties.getTopic().getBetslip(), Consumed.with(Serdes.Long(), BetSlipSnapshotSerde.serde()));


        return builder.build();
    }

    private void addGlobalBetSlipStore(StreamsBuilder builder, String betslip) {
        builder.globalTable(betslip, Materialized.<Long, BetSlipSnapshot, KeyValueStore<Bytes, byte[]>>as("bet-slip-store")
                .withKeySerde(Serdes.Long())
                .withValueSerde(BetSlipSnapshotSerde.serde()));
    }

    private void addGlobalMatchStore(StreamsBuilder builder, String match) {
        builder.globalTable(match, Materialized.<Long, MatchSnapshot, KeyValueStore<Bytes, byte[]>>as("match-store")
                .withKeySerde(Serdes.Long())
                .withValueSerde(MatchSnapshotSerde.serde()));
    }


}
