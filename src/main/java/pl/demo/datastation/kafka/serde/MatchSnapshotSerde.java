package pl.demo.datastation.kafka.serde;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import pl.demo.datastation.model.betslip.BetSlipSnapshot;
import pl.demo.datastation.model.match.MatchSnapshot;

import java.util.HashMap;
import java.util.Map;

public class MatchSnapshotSerde {
    private static final Map<String, Object> serdeProps = new HashMap<>();
    private static final Serializer<MatchSnapshot> matchSnapshotSerializer = new JsonPOJOSerializer<MatchSnapshot>();
    private static final Deserializer<MatchSnapshot> matchSnapshotDeserializer = new JsonPOJODeserializer<MatchSnapshot>();

    public static Serde<MatchSnapshot> serde(){
        serdeProps.put("JsonPOJOClass", MatchSnapshot.class);
        matchSnapshotSerializer.configure(serdeProps, false);
        matchSnapshotDeserializer.configure(serdeProps, false);
        Serde<MatchSnapshot> matchSnapshotSerde = Serdes.serdeFrom(matchSnapshotSerializer, matchSnapshotDeserializer);
        return matchSnapshotSerde;
    }
}
