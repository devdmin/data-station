package pl.demo.datastation.kafka.serde;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import pl.demo.datastation.model.betslip.BetSlipSnapshot;

import java.util.HashMap;
import java.util.Map;

public class BetSlipSnapshotSerde {
    private static final Map<String, Object> serdeProps = new HashMap<>();
    private static final Serializer<BetSlipSnapshot> betSlipSnapshotSerializer = new JsonPOJOSerializer<BetSlipSnapshot>();
    private static final Deserializer<BetSlipSnapshot> betSlipSnapshotDeserializer = new JsonPOJODeserializer<BetSlipSnapshot>();

    public static Serde<BetSlipSnapshot> serde(){
        serdeProps.put("JsonPOJOClass", BetSlipSnapshot.class);
        betSlipSnapshotSerializer.configure(serdeProps, false);
        betSlipSnapshotDeserializer.configure(serdeProps, false);
        Serde<BetSlipSnapshot> betSlipSnapshotSerde = Serdes.serdeFrom(betSlipSnapshotSerializer, betSlipSnapshotDeserializer);
        return betSlipSnapshotSerde;
    }
}
