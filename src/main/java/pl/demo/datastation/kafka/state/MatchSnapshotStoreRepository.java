package pl.demo.datastation.kafka.state;

import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import pl.demo.datastation.model.match.MatchSnapshot;

@Component
public class MatchSnapshotStoreRepository {
    private static final Logger LOG = LoggerFactory.getLogger(MatchSnapshotStoreRepository.class);

    private final String topicName = "match-store";
    private final ReadOnlyKeyValueStore<Long, MatchSnapshot> store;
    private final KafkaTemplate<Long, MatchSnapshot> kafkaTemplate;

    @Autowired
    public MatchSnapshotStoreRepository(ReadOnlyKeyValueStore<Long, MatchSnapshot> store, KafkaTemplate<Long, MatchSnapshot> kafkaTemplate) {
        this.store = store;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void save(MatchSnapshot matchSnapshot) {
        LOG.info(matchSnapshot.toString());
        kafkaTemplate.send(topicName, matchSnapshot.getId(), matchSnapshot);
    }

    public MatchSnapshot find(Long id) {
        return store.get(id);
    }
}
