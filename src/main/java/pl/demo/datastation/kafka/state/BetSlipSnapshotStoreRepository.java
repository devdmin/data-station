package pl.demo.datastation.kafka.state;

import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import pl.demo.datastation.model.betslip.BetSlipSnapshot;

@Component
public class BetSlipSnapshotStoreRepository {

    private final String topicName = "bet-slip-store";
    private final ReadOnlyKeyValueStore<Long, BetSlipSnapshot> store;
    private final KafkaTemplate<Long, BetSlipSnapshot> kafkaTemplate;

    @Autowired
    public BetSlipSnapshotStoreRepository(ReadOnlyKeyValueStore<Long, BetSlipSnapshot> store, KafkaTemplate<Long, BetSlipSnapshot> kafkaTemplate) {
        this.store = store;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void save(BetSlipSnapshot betSlipSnapshot) {
        kafkaTemplate.send(topicName, betSlipSnapshot.getId(), betSlipSnapshot);
    }

    public BetSlipSnapshot find(Long id) {
        return store.get(id);
    }
}
