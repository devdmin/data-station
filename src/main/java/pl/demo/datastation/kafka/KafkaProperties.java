package pl.demo.datastation.kafka;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
@ConfigurationProperties("kafka")
public class KafkaProperties {
    private String servers;
    private TopicConfig topic;
    private String applicationId;
    private StoreConfig store;
    public String getServers() {
        return servers;
    }

    public void setServers(String servers) {
        this.servers = servers;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public StoreConfig getStore() {
        return store;
    }

    public void setStore(StoreConfig store) {
        this.store = store;
    }

    public TopicConfig getTopic(){
        return topic;
    }

    public void setTopic(TopicConfig topic) {
        this.topic = topic;
    }

    public static class TopicConfig {
        private String main, output, betslip, match;

        public String getMain() {
            return main;
        }
        public String getOutput() {return  output;}
        public void setMain(String main) {
            this.main = main;
        }
        public void setOutput(String output) {this.output = output; }

        public String getBetslip() {
            return betslip;
        }

        public void setBetslip(String betslip) {
            this.betslip = betslip;
        }

        public String getMatch() {
            return match;
        }

        public void setMatch(String match) {
            this.match = match;
        }
    }
    public static class StoreConfig {
        private String topic, match, betslip;

        public String getTopic() {
            return topic;
        }

        public void setTopic(String topic) {
            this.topic = topic;
        }

        public String getMatch() {
            return match;
        }

        public void setMatch(String match) {
            this.match = match;
        }

        public String getBetslip() {
            return betslip;
        }

        public void setBetslip(String betslip) {
            this.betslip = betslip;
        }
    }

    public Properties prepareStoreProducerProperties(){
        Properties properties = new Properties();
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, "store");
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
        return properties;
    }

    public Properties prepareMatchStoreProducerProperties(){
        Properties properties = new Properties();
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, "match-store");
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
        return properties;
    }

    public Properties prepareBetSlipStoreProducerProperties(){
        Properties properties = new Properties();
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, "bet-slip-store");
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
        return properties;
    }
}
