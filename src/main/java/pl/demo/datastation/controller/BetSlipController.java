package pl.demo.datastation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.demo.datastation.model.TipSnapshot;
import pl.demo.datastation.model.betslip.BetSlipSnapshot;
import pl.demo.datastation.service.BetSlipService;

import java.util.Optional;

@RestController
@RequestMapping(value = "/betSlip")
public class BetSlipController {
    private static final Logger LOG = LoggerFactory.getLogger(BetSlipController.class);

    private BetSlipService betSlipService;

    @Autowired
    public BetSlipController(BetSlipService betSlipService) {
        this.betSlipService = betSlipService;
    }


    @PostMapping("/add")
    public ResponseEntity<BetSlipSnapshot> add(@RequestBody BetSlipSnapshot betSlipSnapshot) {
        betSlipService.save(betSlipSnapshot);
        return new ResponseEntity<>(betSlipSnapshot, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BetSlipSnapshot> find(@PathVariable("id") Long id) {
        return Optional.ofNullable(betSlipService.find(id))
                .map(sn -> {
                    return new ResponseEntity<BetSlipSnapshot>(sn, HttpStatus.FOUND);
                })
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/{betSlipId}/addTip")
    public ResponseEntity<BetSlipSnapshot> addTip(@PathVariable("betSlipId") Long id, @RequestBody TipSnapshot tip) {
        return Optional.ofNullable(betSlipService.addTip(id, tip))
                .map(sn -> {
                    return new ResponseEntity<BetSlipSnapshot>(sn, HttpStatus.OK);
                })
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/{Id}/isWon")
    public ResponseEntity<Boolean> isWon(@PathVariable("Id") Long id) {
        return Optional.ofNullable(betSlipService.isWon(id))
                .map(b -> new ResponseEntity<Boolean>(b, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
