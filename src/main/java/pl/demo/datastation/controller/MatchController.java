package pl.demo.datastation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.demo.datastation.model.match.MatchSnapshot;
import pl.demo.datastation.service.MatchService;

import java.util.Optional;

@RestController
@RequestMapping(value = "/match")
public class MatchController {
    private static final Logger LOG = LoggerFactory.getLogger(MatchController.class);

    private MatchService matchService;

    @Autowired
    public MatchController(MatchService matchService) {
        this.matchService = matchService;
    }

    @PostMapping("/add")
    public ResponseEntity<MatchSnapshot> add(@RequestBody MatchSnapshot matchSnapshot){
        LOG.info(matchSnapshot.toString());
        matchService.save(matchSnapshot);
        return new ResponseEntity<MatchSnapshot>(matchSnapshot, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<MatchSnapshot> find(@PathVariable("id") Long id){
        return Optional.ofNullable(matchService.find(id))
                .map(sn -> {return new ResponseEntity<MatchSnapshot>(sn, HttpStatus.FOUND);})
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/test")
    public ResponseEntity<MatchSnapshot> test(){
        return new ResponseEntity<MatchSnapshot>(HttpStatus.OK);
    }
}
