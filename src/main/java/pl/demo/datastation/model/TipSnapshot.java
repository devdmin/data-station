package pl.demo.datastation.model;

import pl.demo.datastation.model.match.MatchSnapshot;

import java.util.Objects;

public class TipSnapshot {
    private MatchSnapshot match;
    private WinningTeam winningTeam;

    public TipSnapshot() {
    }

    public TipSnapshot(MatchSnapshot match, WinningTeam winningTeam) {
        this.match = match;
        this.winningTeam = winningTeam;
    }

    public MatchSnapshot getMatch() {
        return match;
    }

    public void setMatch(MatchSnapshot match) {
        this.match = match;
    }

    public WinningTeam getWinningTeam() {
        return winningTeam;
    }

    public void setWinningTeam(WinningTeam winningTeam) {
        this.winningTeam = winningTeam;
    }

    public Tip toDomain(){
        return new Tip(match.toDomain(), winningTeam);
    }

    @Override
    public String toString() {
        return "TipSnapshot{" +
                "match=" + match +
                ", winningTeam=" + winningTeam +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TipSnapshot that = (TipSnapshot) o;
        return Objects.equals(match, that.match) &&
                winningTeam == that.winningTeam;
    }

    @Override
    public int hashCode() {
        return Objects.hash(match, winningTeam);
    }
}
