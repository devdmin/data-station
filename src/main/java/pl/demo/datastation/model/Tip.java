package pl.demo.datastation.model;

import pl.demo.datastation.model.WinningTeam;
import pl.demo.datastation.model.match.Match;

import java.util.Objects;

public class Tip {
    private Match match;
    private WinningTeam winningTeam;

    public Tip(Match match, WinningTeam winningTeam) {
        this.match = match;
        this.winningTeam = winningTeam;
    }

    public Match getMatch() {
        return match;
    }

    public WinningTeam getWinningTeam() {
        return winningTeam;
    }

    public TipSnapshot toSnapshot() {
        return new TipSnapshot(match.toSnapshot(), winningTeam);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tip tip = (Tip) o;
        return Objects.equals(match, tip.match) &&
                winningTeam == tip.winningTeam;
    }

    @Override
    public int hashCode() {
        return Objects.hash(match, winningTeam);
    }
}
