package pl.demo.datastation.model.betslip;

import pl.demo.datastation.model.Tip;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class BetSlip {
    private long id;
    private Optional<Set<Tip>> tips;
    private BigDecimal stake;

    public BetSlip(long id, Optional<Set<Tip>> tips, BigDecimal stake) {
        this.id = id;
        this.tips = tips;
        this.stake = stake;
    }

    public boolean isEmpty(){
        if(tips.isPresent())
            return tips.get().isEmpty();
        else
            return true;
    }

    public BigDecimal multiplyRateValues(){

        BigDecimal value = BigDecimal.ONE;
        if(tips.isPresent()) {
            for (Tip tip : tips.get()) {
                value = value.multiply(tip.getMatch().getRateOfTeam(tip.getWinningTeam()));
            }
        }
        return value;
    }

    public boolean isWon() {
        if(tips.isPresent()) {
            for (Tip tip : tips.get()) {
                if (!tip.getMatch().isWinning(tip.getWinningTeam())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public BetSlip addTip(Tip tip) {
        if (tips.isPresent()) {
            tips.get().add(tip);
        }else{
            Set<Tip> set = new HashSet<>();
            set.add(tip);
            tips = Optional.of(set);
        }
        return this;
    }

    public BigDecimal getPayout(){
        if (isWon())
            return multiplyRateValues().multiply(stake);
        else
            return BigDecimal.ZERO;
    }

    public BetSlipSnapshot toSnapshot(){
        if (tips.isPresent()) {

            return new BetSlipSnapshot(id, tips.get().stream().map(tip -> tip.toSnapshot()).collect(Collectors.toSet()), stake);
        }else{
            return new BetSlipSnapshot(id, stake);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BetSlip betSlip = (BetSlip) o;
        return id == betSlip.id &&
                Objects.equals(tips, betSlip.tips) &&
                Objects.equals(stake, betSlip.stake);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tips, stake);
    }
}
