package pl.demo.datastation.model.betslip;

import pl.demo.datastation.model.Tip;
import pl.demo.datastation.model.TipSnapshot;
import pl.demo.datastation.model.WinningTeam;
import pl.demo.datastation.model.match.Match;
import pl.demo.datastation.model.match.MatchSnapshot;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class BetSlipSnapshot {
    private long id;
    private Set<TipSnapshot> tips;
    private BigDecimal stake;

    public BetSlipSnapshot() {
    }

    public BetSlipSnapshot(long id, BigDecimal stake) {
        this.id = id;
        this.stake = stake;
    }

    public BetSlipSnapshot(long id, Set<TipSnapshot> tips, BigDecimal stake) {
        this.id = id;
        this.tips = tips;
        this.stake = stake;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<TipSnapshot> getTips() {
        return tips;
    }

    public void setTips(Set<TipSnapshot> tips) {
        this.tips = tips;
    }

    public BigDecimal getStake() {
        return stake;
    }

    public void setStake(BigDecimal stake) {
        this.stake = stake;
    }

    @Override
    public String toString() {
        return "BetSlipSnapshot{" +
                "id=" + id +
                ", tips=" + tips +
                ", stake=" + stake +
                '}';
    }

    public BetSlip toDomain(){
        Set<Tip> convertedTips = null;
        if (tips != null) convertedTips = tips.stream().map(tip -> tip.toDomain()).collect(Collectors.toSet());
        return new BetSlip(id, Optional.ofNullable(convertedTips), stake);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BetSlipSnapshot that = (BetSlipSnapshot) o;
        return id == that.id &&
                Objects.equals(tips, that.tips) &&
                Objects.equals(stake, that.stake);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tips, stake);
    }
}
