package pl.demo.datastation.model;

public enum WinningTeam {
    FIRST, SECOND, ANY;
}
