package pl.demo.datastation.model.match;

public class ScoreIsNotKnownException extends RuntimeException {
    public ScoreIsNotKnownException() {
    }

    public ScoreIsNotKnownException(String s) {
        super(s);
    }
}
