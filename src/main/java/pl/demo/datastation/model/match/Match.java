package pl.demo.datastation.model.match;

import pl.demo.datastation.model.WinningTeam;


import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public final class Match {
    private Long id;
    private String team1;
    private String team2;
    private List<BigDecimal> rates;
    private Optional<Integer> goalsForFirstTeam, goalsForSecondTeam;

    public Match(Long id, String team1, String team2, List<BigDecimal> rates, Optional<Integer> goalsForFirstTeam, Optional<Integer> goalsForSecondTeam) {
        this.id = id;
        this.team1 = team1;
        this.team2 = team2;
        this.rates = rates;
        this.goalsForFirstTeam = goalsForFirstTeam;
        this.goalsForSecondTeam = goalsForSecondTeam;
    }

    public boolean isWinning(WinningTeam winningTeam) {
        if (!goalsForFirstTeam.isPresent() || !goalsForSecondTeam.isPresent()) {
            throw new ScoreIsNotKnownException();
        }
        else {
            switch (winningTeam) {
                case FIRST:
                    return goalsForFirstTeam.get() > goalsForSecondTeam.get();
                case SECOND:
                    return goalsForSecondTeam.get() > goalsForFirstTeam.get();
                default:
                    return goalsForFirstTeam.get() == goalsForSecondTeam.get();
            }
        }
    }

    public WinningTeam getWinningTeam(){
        if (isWinning(WinningTeam.FIRST)) {
            return WinningTeam.FIRST;
        } else if (isWinning(WinningTeam.SECOND)) {
            return WinningTeam.SECOND;
        }else{
            return WinningTeam.ANY;
        }
    }

    public Match updateScore(Optional<Integer> goalsForFirstTeam, Optional<Integer> goalsForSecondTeam) {
        return new Match(id, team1, team2, rates, goalsForFirstTeam, goalsForSecondTeam);
    }

    public BigDecimal getWinningRate(){
        return getRateOfTeam(getWinningTeam());
    }

    public BigDecimal getRateOfTip(WinningTeam tip){
        if(tip.equals(getWinningTeam()))
            return getWinningRate();
        else
            return new BigDecimal("0.00");
    }

    public BigDecimal getRateOfTeam(WinningTeam team) {
        switch (team) {
            case FIRST:
                return rates.get(0);
            case SECOND:
                return rates.get(2);
            default:
                return rates.get(1);
        }
    }

    public MatchSnapshot toSnapshot(){
        if (goalsForFirstTeam.isPresent() && goalsForSecondTeam.isPresent())
            return new MatchSnapshot(id, team1, team2, rates, goalsForFirstTeam.get(), goalsForSecondTeam.get());
        else
            return new MatchSnapshot(id, team1, team2, rates);
    }


    @Override
    public String toString() {
        return "Match{" +
                "id=" + id +
                ", team1='" + team1 + '\'' +
                ", team2='" + team2 + '\'' +
                ", rates=" + rates +
                ", goalsForFirstTeam=" + goalsForFirstTeam +
                ", goalsForSecondTeam=" + goalsForSecondTeam +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Match match = (Match) o;
        return Objects.equals(id, match.id) &&
                Objects.equals(team1, match.team1) &&
                Objects.equals(team2, match.team2) &&
                Objects.equals(rates, match.rates) &&
                Objects.equals(goalsForFirstTeam, match.goalsForFirstTeam) &&
                Objects.equals(goalsForSecondTeam, match.goalsForSecondTeam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, team1, team2, rates, goalsForFirstTeam, goalsForSecondTeam);
    }
}
