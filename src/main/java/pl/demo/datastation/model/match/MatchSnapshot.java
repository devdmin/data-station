package pl.demo.datastation.model.match;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class MatchSnapshot {
    private Long id;
    private String team1;
    private String team2;
    private List<BigDecimal> rates;
    private int goalsForFirstTeam, goalsForSecondTeam;


    public MatchSnapshot() {
    }

    public MatchSnapshot(Long id, String team1, String team2, List<BigDecimal> rates) {
        this.id = id;
        this.team1 = team1;
        this.team2 = team2;
        this.rates = rates;
    }

    public MatchSnapshot(Long id, String team1, String team2, List<BigDecimal> rates, int goalsForFirstTeam, int goalsForSecondTeam) {
        this.id = id;
        this.team1 = team1;
        this.team2 = team2;
        this.rates = rates;
        this.goalsForFirstTeam = goalsForFirstTeam;
        this.goalsForSecondTeam = goalsForSecondTeam;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public List<BigDecimal> getRates() {
        return rates;
    }

    public void setRates(List<BigDecimal> rates) {
        this.rates = rates;
    }

    public int getGoalsForFirstTeam() {
        return goalsForFirstTeam;
    }

    public void setGoalsForFirstTeam(int goalsForFirstTeam) {
        this.goalsForFirstTeam = goalsForFirstTeam;
    }

    public int getGoalsForSecondTeam() {
        return goalsForSecondTeam;
    }

    public void setGoalsForSecondTeam(int goalsForSecondTeam) {
        this.goalsForSecondTeam = goalsForSecondTeam;
    }

    public Match toDomain(){
        return new Match(id, team1, team2, rates, Optional.ofNullable(goalsForFirstTeam), Optional.ofNullable(goalsForSecondTeam));
    }

    @Override
    public String toString() {
        return "MatchSnapshot{" +
                "id=" + id +
                ", team1='" + team1 + '\'' +
                ", team2='" + team2 + '\'' +
                ", rates=" + rates +
                ", goalsForFirstTeam=" + goalsForFirstTeam +
                ", goalsForSecondTeam=" + goalsForSecondTeam +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatchSnapshot that = (MatchSnapshot) o;
        return goalsForFirstTeam == that.goalsForFirstTeam &&
                goalsForSecondTeam == that.goalsForSecondTeam &&
                Objects.equals(id, that.id) &&
                Objects.equals(team1, that.team1) &&
                Objects.equals(team2, that.team2) &&
                Objects.equals(rates, that.rates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, team1, team2, rates, goalsForFirstTeam, goalsForSecondTeam);
    }
}
