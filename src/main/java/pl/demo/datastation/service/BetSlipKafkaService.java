package pl.demo.datastation.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.demo.datastation.kafka.state.BetSlipSnapshotStoreRepository;
import pl.demo.datastation.model.Tip;
import pl.demo.datastation.model.TipSnapshot;
import pl.demo.datastation.model.betslip.BetSlip;
import pl.demo.datastation.model.betslip.BetSlipSnapshot;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class BetSlipKafkaService implements BetSlipService {
    private static final Logger LOG = LoggerFactory.getLogger(BetSlipKafkaService.class);

    private BetSlipSnapshotStoreRepository repository;

    @Autowired
    public BetSlipKafkaService(BetSlipSnapshotStoreRepository repository) {
        this.repository = repository;
    }

    public BetSlipSnapshot find(Long id) {
        return repository.find(id);
    }

    public BetSlipSnapshot save(BetSlipSnapshot betSlipSnapshot) {
        repository.save(betSlipSnapshot);
        return betSlipSnapshot;
    }
    public BetSlipSnapshot addTip(Long betSlipId, TipSnapshot tip) {
        BetSlip betSlip = find(betSlipId)
                .toDomain()
                .addTip(tip.toDomain());
        LOG.info(betSlip.toSnapshot().toString());
        return save(betSlip.toSnapshot());
    }
    public BigDecimal countPossiblePayout(Long id) {
       return null;
    }

    public boolean isWon(Long id) {
        return find(id).toDomain().isWon();
    }
}
