package pl.demo.datastation.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.demo.datastation.kafka.state.MatchSnapshotStoreRepository;
import pl.demo.datastation.model.match.MatchSnapshot;

@Service
@Transactional
public class MatchKafkaService implements MatchService {
    private static final Logger LOG = LoggerFactory.getLogger(MatchKafkaService.class);
    private MatchSnapshotStoreRepository repository;

    @Autowired
    public MatchKafkaService(MatchSnapshotStoreRepository repository) {
        this.repository = repository;
    }

    public MatchSnapshot find(Long id) {
        return repository.find(id);
    }

    public MatchSnapshot save(MatchSnapshot matchSnapshot) {
        LOG.info(matchSnapshot.toString());
        repository.save(matchSnapshot);
        return matchSnapshot;
    }
}
