package pl.demo.datastation.service;

import pl.demo.datastation.model.Tip;
import pl.demo.datastation.model.TipSnapshot;
import pl.demo.datastation.model.betslip.BetSlipSnapshot;

import java.math.BigDecimal;

public interface BetSlipService {
    BetSlipSnapshot find(Long id);
    BetSlipSnapshot save(BetSlipSnapshot betSlipSnapshot);
    BetSlipSnapshot addTip(Long betSlipId, TipSnapshot tip);
    BigDecimal countPossiblePayout(Long id);
    boolean isWon(Long id);
}
