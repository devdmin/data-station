package pl.demo.datastation.service;

import pl.demo.datastation.model.match.MatchSnapshot;

import java.util.Optional;

public interface MatchService {
    MatchSnapshot find(Long id);
    MatchSnapshot save(MatchSnapshot matchSnapshot);
}
