package pl.demo.datastation.kafka.listener

import org.apache.kafka.common.serialization.Deserializer
import org.apache.kafka.common.serialization.Serdes
import org.springframework.test.web.servlet.MockMvcBuilder
import pl.demo.datastation.kafka.KafkaProperties
import spock.lang.*;

class ResultStreamTopologyTest extends Specification{
//    private static final String TOPIC = 'message-topic'
//    public static final Deserializer<String> STRING_DESERIALIZER = Serdes.String().deserializer()
//
//    void setup(){
//        KafkaProperties kafkaProperties = setupKafkaProperties()
//        Properties properties = setupKafkaProperties(kafkaProperties)
//
//        ResultStreamTopology resultStreamTopology = new ResultStreamTopology(kafkaProperties)
//    }
    @Unroll
    def "maximum of #a and #b equals #c"(int a, int b, int c) {
        expect:
        Math.max(a, b) == c

        where:
        a | b || c
        1 | 3 || 3
        7 | 4 || 7
        0 | 0 || 0
    }
}
