package pl.demo.datastation.controller


import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import pl.demo.datastation.model.match.MatchSnapshot
import pl.demo.datastation.service.MatchService

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import spock.lang.Specification


class MatchControllerTest extends Specification {
    MockMvc mockMvc;
    MatchController matchController
    def setup() {
        matchController = new MatchController()
        mockMvc = org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup(matchController).build()
    }

    def "add request should return proper status"(){
        when:
        def matchService =Mock(MatchService)
        matchController.matchService = matchService
        def response = mockMvc.perform(post("/match/add")
                                .content("{\"id\":3,\"team1\":\"bayern\",\"team2\":\"hamburg\",\"rates\":[1.60,3.8,5.7]}")
                                .contentType(MediaType.APPLICATION_JSON))



        then:
        response.andExpect(status().isOk())
    }

    def "find request should return MatchSnapshot"(){
        when:
        matchController.matchService = Stub(MatchService) {
            find(_) >> new MatchSnapshot()
        }

        def reponse = mockMvc.perform(get("/match/find/3"))

        then:
        reponse.andExpect(status().isFound())
    }
}