package pl.demo.datastation.controller

import org.springframework.test.web.servlet.MockMvc
import org.springframework.http.MediaType
import pl.demo.datastation.model.TipSnapshot
import pl.demo.datastation.model.betslip.BetSlipSnapshot
import pl.demo.datastation.model.match.Match
import pl.demo.datastation.service.BetSlipService
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import spock.lang.Specification

class BetSlipControllerTest extends Specification{
    MockMvc mockMvc;

    BetSlipController betSlipController;

    def setup() {
        betSlipController = new BetSlipController()
        mockMvc = org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup(betSlipController).build()
    }

    def "add request should return proper status"(){
        when:
        def betSlipService = Mock(BetSlipService)
        betSlipController.betSlipService = betSlipService
        def response = mockMvc.perform(post("/betSlip/add").content("{\"id\":3,\"stake\":10.00}")
                .contentType(MediaType.APPLICATION_JSON))

        then:
        response.andExpect(status().isOk())
    }

    def "find request should return proper status"(){
        when:
        betSlipController.betSlipService = Stub(BetSlipService) {
            find(3) >> new BetSlipSnapshot()
        }

        def response = mockMvc.perform(get("/betSlip/3"))

        then:
        response.andExpect(status().isFound())
    }

    def "request should return a betslip with an added match"(){
        when:
        betSlipController.betSlipService = Stub(BetSlipService) {
            addTip(_,_) >> new BetSlipSnapshot()
        }

        def response = mockMvc.perform(post("/betSlip/3/addTip")
                .content("{\"match\": {\"id\":3,\"stake\":10.00}, \"winningTeam\": \"FIRST\"}")
                .contentType(MediaType.APPLICATION_JSON))


        then:
        response.andExpect(status().isOk())
    }

    def "request should return true or false if a bet slip is won"(){
        when:
        betSlipController.betSlipService = Stub(BetSlipService){
            isWon(_) >> true
        }

        def response = mockMvc.perform(get("/betSlip/3/isWon"))

        then:
        response.andExpect(status().isOk())

    }
}
