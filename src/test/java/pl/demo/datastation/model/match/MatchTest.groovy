package pl.demo.datastation.model.match

import pl.demo.datastation.model.WinningTeam
import pl.demo.datastation.model.match.MatchSnapshot
import pl.demo.datastation.model.match.ScoreIsNotKnownException
import pl.demo.datastation.model.match.Match
import spock.lang.Specification
import spock.lang.Unroll

class MatchTest extends Specification{
    private Match match;

    void setup(){
        match = new Match(1L, "Team1","Team2", [1.60, 3.8, 5.7], Optional.empty(), Optional.empty())
    }

    def 'isWinning should throw an exception when score is not known'(){
        when:
        match.isWinning(input)

        then:
        thrown(exceptedException)

        where:
        input              ||  exceptedException
        WinningTeam.FIRST  ||  ScoreIsNotKnownException
        WinningTeam.SECOND ||  ScoreIsNotKnownException
        WinningTeam.ANY    ||  ScoreIsNotKnownException
    }

    def 'isWinning should return proper value'(){
        when:
        Match match = match.updateScore(Optional.of(goalsForFirstTeam), Optional.of(goalsForSecondTeam))

        then:
        match.isWinning(requestedTeam) == expectedResult

        where:
        goalsForFirstTeam | goalsForSecondTeam  | requestedTeam      || expectedResult
        1                 | 2                   | WinningTeam.FIRST  || false
        2                 | 0                   | WinningTeam.FIRST  || true
        0                 | 0                   | WinningTeam.FIRST  || false
        5                 | 5                   | WinningTeam.SECOND || false
        1                 | 3                   | WinningTeam.SECOND || true
        1                 | 1                   | WinningTeam.ANY    || true
    }

    def 'getWinningTeam should return proper value'(){
        when:
        Match match = match.updateScore(Optional.of(goalsForFirstTeam), Optional.of(goalsForSecondTeam))

        then:
        match.getWinningTeam() == expectedWinningTeam

        where:
        goalsForFirstTeam | goalsForSecondTeam || expectedWinningTeam
        1                 | 2                  || WinningTeam.SECOND
        2                 | 0                  || WinningTeam.FIRST
        0                 | 0                  || WinningTeam.ANY
        5                 | 5                  || WinningTeam.ANY
    }

    def 'getWinningRateshould throw an exception when score is not known'(){
        when:
        match.getWinningRate()

        then:
        thrown ScoreIsNotKnownException

    }

    def 'getWinningRate should return proper value'(){
        when:
        Match match = match.updateScore(Optional.of(goalsForFirstTeam), Optional.of(goalsForSecondTeam))

        then:
        match.getWinningRate() == expectedWinningRate

        where:
        goalsForFirstTeam | goalsForSecondTeam || expectedWinningRate
        1                 | 2                  || 5.7
        2                 | 0                  || 1.6
        0                 | 0                  || 3.8
        5                 | 5                  || 3.8
    }

    def 'getWinningTeam should throw an exception when score is not known'(){
        when:
        match.getWinningTeam()

        then:
        thrown ScoreIsNotKnownException
    }

    def "getRateOfTip should return proper value"(){
        when:
        Match match = match.updateScore(Optional.of(goalsForFirstTeam), Optional.of(goalsForSecondTeam))

        then:
        match.getRateOfTip(tip) == expectedWinningRate

        where:
        goalsForFirstTeam | goalsForSecondTeam | tip                  || expectedWinningRate
        1                 | 2                  | WinningTeam.SECOND   || 5.7
        2                 | 0                  | WinningTeam.SECOND   || 0.0
        0                 | 0                  | WinningTeam.ANY      || 3.8
        5                 | 5                  | WinningTeam.FIRST    || 0.0
        2                 | 0                  | WinningTeam.FIRST    || 1.6
    }

    def "getRateOfTip should throw an exception when score is not known"(){
        when:
        match.getRateOfTip(WinningTeam.ANY)

        then:
        thrown ScoreIsNotKnownException

    }

    def "getRateOfTeam should return proper value"(){
        when:
        match.getRateOfTeam(team)

        then:
        match.getRateOfTeam(team) == expectedResult

        where:
        team                ||  expectedResult
        WinningTeam.FIRST   ||  1.60
        WinningTeam.ANY     ||  3.80
        WinningTeam.SECOND  ||  5.70
    }
}
