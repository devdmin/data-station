package pl.demo.datastation.model.betslip

import pl.demo.datastation.model.Tip
import pl.demo.datastation.model.WinningTeam
import pl.demo.datastation.model.match.Match
import spock.lang.Specification

class BetSlipTest extends Specification {
    BetSlip betSlip
    Match match
    Match match2

    void setup(){

         match = new Match(1L, "Team1","Team2", [1.60, 3.8, 5.7], Optional.of(4), Optional.of(2))
         match2 = new Match(2L, "Team1","Team2", [7.60, 1.8, 1.05], Optional.of(1), Optional.of(1))


        betSlip = new BetSlip(1L, Optional.of(new HashSet<>()), new BigDecimal("10.00"));

    }

    def 'isEmpty should return true if there is no tips'(){
        when:
        betSlip.isEmpty()
        then:
        true
    }

    def 'multiplyRateValues should return proper value'(){
        when:
        def tips = new HashSet<Tip>()
        tips.add(new Tip(match, WinningTeam.FIRST))
        tips.add(new Tip(match2, WinningTeam.ANY))
        betSlip = new BetSlip(1L, Optional.ofNullable(tips) , new BigDecimal("10.00"));

        then:
        betSlip.multiplyRateValues() == new BigDecimal("1.60").multiply(new BigDecimal("1.80"))
    }

    def 'isWon should return proper value'(){
        when:
        def tips = new HashSet<Tip>()
        tips.add(new Tip(match, firstMatch))
        tips.add(new Tip(match2, secondMatch))
        betSlip = new BetSlip(1L, Optional.ofNullable(tips) , new BigDecimal("10.00"));

        then:
        betSlip.isWon() == expectedResult

        where:
        firstMatch        | secondMatch         || expectedResult
        WinningTeam.FIRST | WinningTeam.ANY     || true
        WinningTeam.ANY   | WinningTeam.SECOND  || false
        WinningTeam.SECOND| WinningTeam.FIRST   || false
    }

    def "getPayout should return proper calculated payout"(){
        when:
        def tips = new HashSet<Tip>()
        tips.add(new Tip(match, WinningTeam.FIRST))
        tips.add(new Tip(match2, WinningTeam.ANY))
        betSlip = new BetSlip(1L, Optional.ofNullable(tips) , new BigDecimal("10.00"));

        then:
        betSlip.getPayout() == new BigDecimal("10.00").multiply(new BigDecimal("1.60")).multiply(new BigDecimal("1.8"))

    }
}
